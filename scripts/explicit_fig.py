#!/usr/bin/env python

# Adapted from Bonsignori 2011 repo

"""
Convert implicit "dotted" Figure S2 panel FASTA seqs into full sequences.
"""

import sys
from collections import defaultdict

def seqdict(path):
    seqs = defaultdict(str)
    seqid = None
    with open(path) as f_in:
        for line in f_in:
            line = line.strip()
            if line.startswith(">"):
                seqid = line[1:]
            else:
                seqs[seqid] += line
    return seqs

def explicit(path, path_out):
    seqs = seqdict(path)
    ref = list(seqs.values())[0]
    with open(path_out, "w") as f_out:
        for seqid, seq in seqs.items():
            out = "".join([b if b != "." else br for b, br in zip(seq, ref)])
            f_out.write(f">{seqid}\n{out}\n")

if __name__ == "__main__":
    explicit(sys.argv[1], sys.argv[2])
