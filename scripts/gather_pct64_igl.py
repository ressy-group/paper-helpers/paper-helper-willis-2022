#!/usr/bin/env python

"""
Put the various PCT64 iGL sequences into one FASTA
"""

import sys
sys.path.append("scripts")
from explicit_fig import seqdict

def pad(txt):
    return "-" * 98 + txt + "-" * 11

def gather_pct64_igl(figs2a, fig3e, tabs5):
    seqs_s2a = seqdict(figs2a)
    print(">iGL_figS2A")
    print(seqs_s2a["iGL"])
    seqs_3e = seqdict(fig3e)
    print(">iGL_fig3E")
    print(pad(seqs_3e["iGL"]))
    seqs_tabs5 = {}
    with open(tabs5) as f_in:
        for line in f_in:
            fields = line.strip().split()
            seqs_tabs5[fields[1]] = fields[2]
    print(">iGL_tableS5_1")
    print(pad(seqs_tabs5["iGL.1"]))
    print(">iGL_tableS5_2")
    print(pad(seqs_tabs5["iGL.2"]))

if __name__ == "__main__":
    gather_pct64_igl(*sys.argv[1:])
