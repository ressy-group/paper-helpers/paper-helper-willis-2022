# Some sequences and metadata from Willis 2022

Some sequences and metadata gleaned from:

Human immunoglobulin repertoire analysis guides design of vaccine priming immunogens targeting HIV V2-apex broadly neutralizing antibody precursors.
Jordan R. Willis et al.
Immunity Volume 55, Issue 11, P2149-2167.e9, November 08, 2022
<https://doi.org/10.1016/j.immuni.2022.09.001>
