panels = S2A_PG9 S2A_PG16 S2A_PCT64 3E_PCT64
panels_out = $(addsuffix _full.fa,$(addprefix parsed/fig,$(panels)))

.SECONDARY:

all: $(panels_out) PCT64_heavy_igl.fasta

PCT64_heavy_igl.fasta: parsed/figS2A_PCT64_full.fa parsed/fig3E_PCT64_full.fa from-paper/tableS5_PCT64.txt
	python scripts/gather_pct64_igl.py $^ > $@

parsed/%_full.fa: parsed/%.fa
	python scripts/explicit_fig.py $^ $@

parsed/%.fa: from-paper/%.txt
	python scripts/parse_fig.py $^ $@
